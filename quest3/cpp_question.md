- 以下程式碼有哪些問題？
- 修改程式碼使之成功 compile。
- 爲什麽輸出的數字沒有按照順序排列？
- 有哪些方法可以讓輸出數字依序排列？
- 哪些情況會使用 thread？（哪些情況不適合）
- 解釋 race condition 與 deadlock。

---

```#include <iostream>
#include <vector>
#include <thread>
#include <chrono>

using namespace std;

void append(vector<int>* list, const int value)
{
  std::chrono::milliseconds waitms(rand() % 1000);
  std::this_thread::sleep_for(waitms);
  list->push_back(value);
}

int main() {
  vector<int> alist;
  const int N = 8;
  for (int i = 0; i < N; i++)
  {
    std::thread t(append, alist, i);
  }
  for (int i = 0; i < N; i++)
  {
  	cout << alist[i] << " ";  
  }
  
  return 0;
}```
