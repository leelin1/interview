## Requirement
1. Golang
    * https://golang.org/dl/
2. Python
    * https://www.python.org/downloads/release/python-392/
3. Build Tools for Visual Studio 2019
    * https://visualstudio.microsoft.com/zh-hant/downloads/
    * All downloads ->  Visual Studio 2019 tools ->  Build Tools for Visual Studio 2019