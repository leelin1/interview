#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

std::string vecJoin(std::vector<std::string> vec)
{
    std::string itemStr = "";
    std::string delim = "";
    for (const auto item : vec)
    {
        itemStr += delim + item;
        delim = ", ";
    }

    return itemStr;
}

int main(int, char **)
{
    std::string line;
    std::ifstream inFile("../dataset/data1.txt");
    if (inFile.is_open())
    {
        while (std::getline(inFile, line))
        {
            size_t start;
            size_t end = 0;
            std::vector<std::string> vec;

            while ((start = line.find_first_not_of(",", end)) != std::string::npos)
            {
                end = line.find(",", start);
                vec.push_back(line.substr(start, end - start));
            }

            // Do something here to process vector
        }
        inFile.close();

        // Do something here to print result
    }
    else
    {
        std::cout << "Unable to open the file" << std::endl;
    }

    return 0;
}