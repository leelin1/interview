package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func csv2Nested(lines []string) {
	fmt.Println(lines)
}

func main() {
	f, err := os.Open("../dataset/data2.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	lines := []string{}
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	csv2Nested(lines)
}
